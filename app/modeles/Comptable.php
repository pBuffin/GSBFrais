<?php

namespace App\modeles;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use DB;

class Comptable extends Model {

//    verification des identifiant pour le comptable
    public function getConnexion($login, $pwd) {
        $connected = false;
        $comptable = DB::table('comptable')
                ->select()
                ->where('login', '=', $login)
                ->first();
        if ($comptable) {
            if ($comptable->mdp == $pwd) {
                Session::put('id', $comptable->id);
                Session::put('nom', $comptable->nom);
                Session::put('prenom', $comptable->prenom);
                $connected = true;
            }
        }
        return $connected;
    }
//requete contenant la liste des visiteur pour le comptable
    public function listeVisiteur() {
        $listeVisiteur = DB::table('visiteur')
                ->select('id', 'nom', 'prenom')
                ->get();
        return $listeVisiteur;
    }
//requete contenant le nom et le prenom du visiteur en fonction de son id
    public function getLeVisiteur($idVisiteur) {
        $leVisiteurtemp = DB::table('visiteur')
                ->select('nom', 'prenom')
                ->where('id', '=', $idVisiteur)
                ->first();

        $leVisiteur = $leVisiteurtemp->prenom . " " . $leVisiteurtemp->nom;
        return $leVisiteur;
    }
//rectification des frais forfait par le comptable
    public function updateFicheForfait($etape, $kilo, $nuit, $resto, $date, $id) {
        $ligne = DB::table('lignefraisforfait')
                ->where([['idVisiteur', '=', $id],
                    ['mois', '=', $date],
                    ['idFraisForfait', '=', 'ETP']
                ])
                ->update(['quantite' => $etape]);

        $ligne = DB::table('lignefraisforfait')
                ->where([['idVisiteur', '=', $id],
                    ['mois', '=', $date],
                    ['idFraisForfait', '=', 'KM']
                ])
                ->update(['quantite' => $kilo]);

        $ligne = DB::table('lignefraisforfait')
                ->where([['idVisiteur', '=', $id],
                    ['mois', '=', $date],
                    ['idFraisForfait', '=', 'NUI']
                ])
                ->update(['quantite' => $nuit]);

        $ligne = DB::table('lignefraisforfait')
                ->where([['idVisiteur', '=', $id],
                    ['mois', '=', $date],
                    ['idFraisForfait', '=', 'REP']
                ])
                ->update(['quantite' => $resto]);
    }

//    requete pour les informations de compte pour le comptable
    public function getCompte($id) {

        $compte = DB::table('Comptable')
                ->select()
                ->where('id', '=', $id)
                ->first();

        return $compte;
    }

//    validation de la fiche et calcule du montant total 
    public function valfiche($id, $date, $ligneForfait, $ligneHorsForfait) {
        
              $base = DB::table('fraisforfait')
                ->select('id','montant')
                ->pluck('montant','id');
              
          $montantHF=0;
          
        foreach ($ligneHorsForfait as $key => $value) {
//            les frais hors forfait marqué refusé ne sont pas prit en compte
            if(!(substr($value->libelle, 0, 8)== 'REFUSE :') ){        
            $montantHF = $montantHF + $value->montant;
            }
        }       
//        calcule du montant de chaque frais en fonction du montant de base inscrit en base de données
        $etp = $ligneForfait['Forfait Etape'] * $base['ETP'];
        $km = $ligneForfait['Frais Kilométrique'] * $base['KM'];
        $nui = $ligneForfait['Nuitée Hôtel'] * $base['NUI'];
        $rep = $ligneForfait['Repas Restaurant'] * $base['REP'];
        
        $montantTotale =  $etp + $km + $nui + $rep + $montantHF;
        $error="";
        $error = DB::table('fichefrais')
                ->where([['idVisiteur', '=', $id],
                    ['mois', '=', $date],
                ])
                ->update(['idEtat' => 'VA', 'montantValide' => $montantTotale]);
        return $error;
    }
//la requete contient le nom et le pronom des visiteur ainsi que leurs fiches associées
    public function listerFiche() {

        $listFiche = DB::table('fichefrais', 'visiteur')
                ->join("visiteur", "id", "=", "idVisiteur")
                ->select('visiteur.nom', 'visiteur.prenom','visiteur.id', 'mois', 'nbJustificatifs', 'montantValide', 'dateModif', 'idEtat')
                ->get();
        return $listFiche;
    }
//    fixe l'etat de la fiche a rembourser'
    public function RembourserFiche($mois,$id){
        
        $error = DB::table('fichefrais')
                ->where([['idVisiteur', '=', $id],
                    ['mois', '=', $mois],
                ])
                ->update(['idEtat' => 'RB']);
        
        return $error;       
    }
//    supprime une ligne hors forfait en ajoutant "REFUSER :" au libellé
    public function supprimerHorsForfait($id){
        
        $ligne = DB::table('lignefraishorsforfait')
                ->select('libelle')
                ->where('id', '=', $id)
                ->first();
        
        $libelle = 'REFUSE : ' . $ligne->libelle;
        $error = DB::table('lignefraishorsforfait')
                ->where('id', '=', $id)
                 ->update(['libelle' => $libelle]);
        
        return $error;
        
    }
    
//    reporte un frais hors forfait au mois suivant
    public function reporterHorsForfait($id, $date, $idVisiteur){
//        vérifiaction de l'existance de la fiche du mois suivant'
        $mois = (int) substr($date, 0, 2) + 1;
        $annee = substr($date, 2);     
         if ($mois<10){
            $mois="0" . $mois;
        }elseif($mois == 12){
            $mois = "01";
            $annee = (int) $annee + 1;
        }       
        $date = $mois . $annee;       
        $fiche = DB::table('fichefrais')
                ->select()
                ->where([['idVisiteur', '=', $idVisiteur],
                    ['mois', "=", $date]
                        ])
                ->first(); 
//        si la fiche n'existe pas elle est crée'
        if(!$fiche){        
             $fiche = DB::table('fichefrais')
                ->insert(['idVisiteur' => $idVisiteur, 'mois' => $date, 'nbJustificatifs' => 0, 'montantValide' => 0, 'dateModif' => date('Y-m-d')]);
        }       
        $error = DB::table('lignefraishorsforfait')
                ->where('id', '=', $id)
                ->update(['mois' => $date]);        
        return $error;       
    }
    
//    insertion du nouveau montant pour une ligne hors forfait
    public function actualiserHorsForfait($id,$value){
         
        $error = DB::table('lignefraishorsforfait')
                ->where('id', '=', $id)
                ->update(['montant' => $value]);
        
        return $error;
    }
}
