<?php

namespace App\modeles;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use DB;

class Visiteur extends Model {

//    vérification des identifiant pour un visiteur
    public function getConnexion($login, $pwd) {
        $connected = false;
        $visiteur = DB::table('visiteur')
                ->select()
                ->where('login', '=', $login)
                ->first();

        if ($visiteur) {
            if ($visiteur->mdp == $pwd) {
                Session::put('id', $visiteur->id);
                Session::put('nom', $visiteur->nom);
                Session::put('prenom', $visiteur->prenom);
                $connected = true;
            }
        }
        return $connected;
    }

//    déconnexion du visiteur
    public function getDeconnexion() {
        Session::forget('id');
        Session::forget('nom');
        Session::forget('prenom');
    }

//    la requete contient les mois des fiche du visiteur authentifier
    public function getMois($id) {
        $liste = DB::table('fichefrais')
                ->where('idVisiteur', '=', $id)
                ->pluck('mois');

        $vli = [];

        foreach ($liste as $ligne) {
            $dateLettre = $this->dateLettre($ligne);
            array_push($vli, $dateLettre);
        }
        $lignes = array_combine($liste, $vli);

        return $lignes;
    }

//    transformation du champs mois des fiche en lettre et années
    public function dateLettre($date) {
        $tabmois = ['Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Decembre'];
        $m = (int) substr($date, 0, 2);
        $mois = $tabmois[$m - 1];
        $annee = substr($date, 2);
        $dateLettre = $mois . " " . $annee;

        return $dateLettre;
    }

//    la requete contient si elle existe, la fiche du mois pour un visiteur
    public function verifierFiche($id, $mois) {

        $fiche = DB::table('fichefrais')
                ->select()
                ->where([['idVisiteur', '=', $id],
                    ['mois', "=", $mois]
                        ]
                )
                ->first();
        return $fiche;
    }

//    création de la fiche d'un visiteur pour le mois donnée'
    public function creeFiche($id, $date) {
        $fiche = DB::table('fichefrais')
                ->insert(['idVisiteur' => $id, 'mois' => $date, 'nbJustificatifs' => 0, 'montantValide' => 0, 'dateModif' => date('Y-m-d')]);

        $m = (int) substr($date, 0, 2);
        $mois = $m - 1;

        if ($mois < 10) {
            $mois = "0" . $mois;
        }

        $annee = substr($date, 2);
        $datePrec = $mois . $annee;
        $error = DB::table('fichefrais')
                ->where([['idVisiteur', '=', $id],
                    ['mois', '=', $datePrec],
                ])
                ->update(['idEtat' => 'CL']);

        return $fiche;
    }

//    la requete insert en base de données un forfait étape
    public function setLigneForfaitEtp($etape, $mois, $id) {
        $bool = false;
        $ligne = DB::table('lignefraisforfait')
                ->select()
                ->where([['idVisiteur', '=', $id],
                    ['mois', '=', $mois],
                    ['idFraisForfait', '=', 'ETP']
                ])
                ->first();
        if ($ligne) {
            $ligne = DB::table('lignefraisforfait')
                    ->where([['idVisiteur', '=', $id],
                        ['mois', '=', $mois],
                        ['idFraisForfait', '=', 'ETP']])
                    ->update(['quantite' => $etape + $ligne->quantite]);
        } else {
            $ligne = DB::table('lignefraisforfait')
                    ->insert(['idVisiteur' => $id, 'mois' => $mois, 'idFraisForfait' => 'ETP', 'quantite' => $etape]);
        }
        $bool = true;
        return $bool;
    }
//la requete insert en base de données un forfait frais kilometrique
    public function setLigneForfaitKm($kilo, $mois, $id) {
        $bool = false;
        $ligne = DB::table('lignefraisforfait')
                ->select()
                ->where([['idVisiteur', '=', $id],
                    ['mois', '=', $mois],
                    ['idFraisForfait', '=', 'KM']])
                ->first();
        if ($ligne) {
            $ligne = DB::table('lignefraisforfait')
                    ->where([['idVisiteur', '=', $id],
                        ['mois', '=', $mois],
                        ['idFraisForfait', '=', 'KM']
                    ])
                    ->update(['quantite' => $kilo + $ligne->quantite]);
        } else {
            $ligne = DB::table('lignefraisforfait')
                    ->insert(['idVisiteur' => $id, 'mois' => $mois, 'idFraisForfait' => 'KM', 'quantite' => $kilo]);
        }
        $bool = true;
        return $bool;
    }
//la requete insert en base de données un forfait nuit
    public function setLigneForfaitNuit($nuit, $mois, $id) {
        $bool = false;
        $ligne = DB::table('lignefraisforfait')
                ->select()
                ->where([['idVisiteur', '=', $id],
                    ['mois', '=', $mois],
                    ['idFraisForfait', '=', 'NUI']])
                ->first();
        if ($ligne) {
            $ligne = DB::table('lignefraisforfait')
                    ->where([['idVisiteur', '=', $id],
                        ['mois', '=', $mois],
                        ['idFraisForfait', '=', 'NUI']
                    ])
                    ->update(['quantite' => $nuit + $ligne->quantite]);
        } else {
            $ligne = DB::table('lignefraisforfait')
                    ->insert(['idVisiteur' => $id, 'mois' => $mois, 'idFraisForfait' => 'NUI', 'quantite' => $nuit]);
        }
        $bool = true;
        return $bool;
    }
//la requete insert en base de données un forfait restaurant
    public function setLigneForfaitResto($resto, $mois, $id) {
        $bool = false;
        $ligne = DB::table('lignefraisforfait')
                ->select()
                ->where([['idVisiteur', '=', $id],
                    ['mois', '=', $mois],
                    ['idFraisForfait', '=', 'REP']])
                ->first();
        if ($ligne) {
            $ligne = DB::table('lignefraisforfait')
                    ->where([['idVisiteur', '=', $id],
                        ['mois', '=', $mois],
                        ['idFraisForfait', '=', 'REP']
                    ])
                    ->update(['quantite' => $resto + $ligne->quantite]);
        } else {
            $ligne = DB::table('lignefraisforfait')
                    ->insert(['idVisiteur' => $id, 'mois' => $mois, 'idFraisForfait' => 'REP', 'quantite' => $resto]);
        }
        $bool = true;
        return $bool;
    }
//la requete insert en base de données un hors forfait 
    public function setLigneHorsForfait($id, $mois, $datehf, $libelle, $montant) {
        $bool = false;
        $ligne = DB::table('lignefraishorsforfait')
                ->insert(['idVisiteur' => $id, 'mois' => $mois, 'libelle' => $libelle, 'date' => $datehf, 'montant' => $montant]);
        if ($ligne) {
            $bool = true;
        }
        return $bool;
    }
//la requete contient les lignes forfait de la fiche du mois selection pour le visiteur authentifier
    public function getLigneForfait($mois, $id) {

        $ligneForfait = DB::table('lignefraisforfait', 'fraisforfait')
                ->join('fraisforfait', 'id', '=', 'lignefraisforfait.idFraisForfait')
                ->select('libelle', 'quantite')
                ->where([
                    ['mois', '=', $mois],
                    ['idVisiteur', '=', $id]
                ])
                ->pluck('quantite', 'libelle');
        return $ligneForfait;
    }
//la requete contient les lignes hors forfait de la fiche du mois selection pour le visiteur authentifier
    public function getLigneHorsForfait($mois, $id) {

        $ligneHorsForfait = DB::table('lignefraishorsforfait')
                ->select()
                ->where([
                    ['mois', '=', $mois],
                    ['idVisiteur', '=', $id]])
                ->get();
        return $ligneHorsForfait;
    }

//    la requete contient les information de compte du visiteur authentifier
    public function getCompte($id) {

        $compte = DB::table('Visiteur')
                ->select()
                ->where('id', '=', $id)
                ->first();

        return $compte;
    }

//    la resuete contient le information de la fiche du mois pour le visiteur
    public function getFiche($mois, $idVisiteur) {

        $fiche = DB::table('fichefrais')
                ->where([['idVisiteur', '=', $idVisiteur],
                    ['mois', '=', $mois]])
                ->first();

        return $fiche;
    }

//    la resuete insert le nombre de justificatif indiquer par l'utilisateur
    public function setJustificatif($id, $justificatif, $mois) {


        $nbJustificatifs = DB::table('fichefrais')
                ->select('nbJustificatifs')
                ->where([['idVisiteur', '=', $id],
                    ['mois', '=', $mois]])
                ->pluck('nbJustificatifs');

        $nbTotal = $nbJustificatifs[0] + $justificatif;

        $ligne = DB::table('fichefrais')
                ->where([['idVisiteur', '=', $id],
                    ['mois', '=', $mois],
                ])
                ->update(['nbJustificatifs' => $nbTotal]);
    }
//    la requete contient le nombre de justificatif pour un mois et un visiteur
    public function getJustificatifs($id, $mois){
        
         $nbJustificatifs = DB::table('fichefrais')
                ->select('nbJustificatifs')
                ->where([['idVisiteur', '=', $id],
                    ['mois', '=', $mois]])
                ->pluck('nbJustificatifs');
         
         return  $nbJustificatifs;
    }
//supprime une ligne hors forfait 
    public function suppHorsForfait($id) {

        $error = DB::table('lignefraishorsforfait')
                ->where('id', '=', $id)
                ->delete();
        
        return $error;
    }

}
