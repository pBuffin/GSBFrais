<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Request;
use App\modeles\Visiteur;
use Illuminate\Support\Facades\Session;

class VisiteurControlleur extends Controller {

//    deconnexion du visiteur
    public function getDeconnexion() {
        $erreur = "";
        $visiteur = new Visiteur();
        $connexion = $visiteur->getDeconnexion();
        return view('Accueil', compact('erreur'));
    }

//    selection du mois de la fiche fiche visiteur
    public function getFiche() {
        $visiteur = new Visiteur();
        $id = Session::get('id');
        $lignes = $visiteur->getMois($id);
        $ligneForfait = null;
        $ligneHorsForfait = null;
        $nbJustificatifs=null;
        return view('ficheFraisVisiteur', compact('lignes', 'ligneForfait', 'ligneHorsForfait','nbJustificatifs'));
    }

//    affichage de la fiche de frais du mois en cours
    public function RenseignerFiche() {
        setlocale(LC_TIME, 'fr', 'fr_FR', 'fr_FR.ISO8859-1');
        $annee = strftime('%Y');
        $mois = strftime('%B');
        $titre = "Renseigner ma fiche de frais du mois de " . $mois . " " . $annee;
        $saisie = true;

        return view('SaisieFicheFrais', compact('saisie', 'titre'));
    }
//affichage de l'accueil des visiteur'
    public function Accueil() {
        $titre = "Bienvenue sur l'intranet GSB";
        return view('Visiteur', compact('titre'));
    }

//    enregistrement des frais forfaités
    public function setForfait() {
        $id = Session::get('id');
        $etape = Request::input('etape');
        $kilo = Request::input('kilo');
        $nuit = Request::input('nuit');
        $resto = Request::input('resto');

        setlocale(LC_TIME, 'fr', 'fr_FR', 'fr_FR.ISO8859-1');
        $mois = strftime('%m');
        $annee = strftime('%Y');
        $date = $mois . $annee;

        $bool = false;
        $string = "";
        $visiteur = new visiteur();

        $fiche = $visiteur->verifierFiche($id, $date);
        if (!$fiche) {
            $fiche = $visiteur->creeFiche($id, $date);
        }

        if ($etape) {
            $bool = $visiteur->setLigneForfaitEtp($etape, $date, $id);
        }
        if ($kilo) {
            $bool = $visiteur->setLigneForfaitKm($kilo, $date, $id);
        }
        if ($nuit) {
            $bool = $visiteur->setLigneForfaitNuit($nuit, $date, $id);
        }
        if ($resto) {
            $bool = $visiteur->setLigneForfaitResto($resto, $date, $id);
        }
        if ($bool) {
            $string = "operation valider";
            return view('Visiteur');
        } else {
            return view('erreur');
        }
    }
//enregistrement d'un frais non forfaité'
    public function HorsForfait() {

        $id = Session::get('id');
        $datehf = Request::input('date');
        $libelle = Request::input('libelle');
        $montant = Request::input('montant');
        setlocale(LC_TIME, 'fr', 'fr_FR', 'fr_FR.ISO8859-1');
        $mois = strftime('%m');
        $annee = strftime('%Y');
        $date = $mois . $annee;
        $datehf = date("d-m-Y", strtotime($datehf));
        $datehf = date("Y-m-d", strtotime($datehf));

        $visiteur = new visiteur();

        $fiche = $visiteur->verifierFiche($id, $date);
        if (!$fiche) {
            $fiche = $visiteur->creeFiche($id, $date);
        }
        $bool = $visiteur->setLigneHorsForfait($id, $date, $datehf, $libelle, $montant);
        if ($bool) {
            return view('Visiteur', compact('string'));
        }
    }
//affichage de la fiche du mois pour le visiteur
    public function ficheMois() {
        $visiteur = new visiteur();
        $date = Request::input('mois');
        $id = Session::get('id');
        $lignes = $visiteur->getMois($id);
        $ligneForfait = $visiteur->getLigneForfait($date, $id);
        $dateLettre = $visiteur->dateLettre($date);
        $ligneHorsForfait = $visiteur->getLigneHorsForfait($date, $id);
        $nbJustificatifs= $visiteur->getJustificatifs($id, $date);
        return view('FicheFraisVisiteur', compact('lignes', 'ligneForfait', 'ligneHorsForfait', 'dateLettre','nbJustificatifs'));
    }
//affichage du compte pour le visteur
    public function getCompte() {

        $visiteur = new visiteur();
        $id = Session::get('id');

        $compte = $visiteur->getCompte($id);

        return view('CompteVisiteur', compact('compte'));
    }
//enregistrement du nombres de justificatifs
    public function setJustificatif() {

        $id = Session::get('id');
        $justificatifs = Request::input('justificatif');
        $date = Request::input('mois');
        $visiteur = new Visiteur();
        setlocale(LC_TIME, 'fr', 'fr_FR', 'fr_FR.ISO8859-1');
        $mois = strftime('%m');
        $annee = strftime('%Y');
        $date = $mois . $annee;

        $visiteur->setJustificatif($id, $justificatifs, $date);
        
        return view('Visiteur');
        
    }
//    suppression d'un frais hors forfait'
    public function suppHorsForfait($id){
        
       $visiteur = new visiteur();
       $visiteur->suppHorsForfait($id);
       
       return view('Visiteur');
        
    }
}
