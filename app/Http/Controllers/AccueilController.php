<?php

namespace App\Http\Controllers;

use Request;
use App\Http\Requests;
use App\modeles\Comptable;
use App\modeles\Visiteur;

class AccueilController extends Controller {

    public function getConnexion() {

        $erreur = "";
        $login = Request::input('login');
        $pwd = Request::input('password');
        $role = Request::input('role');

//        vérfication du role qui se connecte
        switch ($role) {
//            connexion d'un visiteur'
            case 'visiteur':
                $visiteur = new Visiteur();
                $connexion = $visiteur->getConnexion($login, $pwd);
                if ($connexion) {
                    return view('visiteur', compact('titre'));
                } else {
                    $erreur = "Login ou Mot de passe incorrect";
                    return view('Accueil', compact('erreur'));
                }
                break;
//          connexion d'un comptable'
            case 'comptable':
                $comptable = new Comptable();
                $connexion = $comptable->getConnexion($login, $pwd);
                if ($connexion) {
                    return view('comptable');
                } else {
                    $erreur = "Login ou Mot de passe incorrect";
                    return view('Accueil', compact('erreur'));
                }
        }
    }

}
