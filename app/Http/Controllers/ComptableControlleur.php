<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Request;
use App\modeles\Comptable;
use App\modeles\Visiteur;
use Illuminate\Support\Facades\Session;

class ComptableControlleur extends Controller {

//    affichage la liste des visiteur au comptable
    public function consulterFiche() {
        $comptable = new Comptable();
        $listeMois = null;
        $ligneForfait = null;
        $ligneHorsForfait = null;
        $idVisiteur = null;
        $ligneVisiteur = $comptable->listeVisiteur();
        $nbJustificatifs=null;
        return view('validationFiche', compact('ligneVisiteur', 'listeMois', 'ligneForfait', 'ligneHorsForfait', 'idVisiteur','nbJustificatifs'));
    }
//affichage de l'accueil comptable'
    public function Accueil() {
        return view('comptable');
    }
//affichage de la liste des mois en fonction du visiteur choisie
    public function getMoisVisiteur() {
        $date = "";
        $fiche = "";
        $visiteur = new Visiteur();
        $comptable = new Comptable();
        $idVisiteur = request::input('visiteur');
        $ligneHorsForfait = null;
        $ligneForfait = null;
        $ligneVisiteur = $comptable->listeVisiteur();
        $listeMois = $visiteur->getMois($idVisiteur);
        $nbJustificatifs=null;
        return view('ValidationFiche', compact('ligneVisiteur', 'listeMois', 'ligneForfait', 'ligneHorsForfait', 'idVisiteur', 'date','nbJustificatifs'));
    }
//affichage de la fiche du visiteur pour le mois selectionner
    public function getFichesVisiteur() {
        $date = Request::input('mois');
        $idVisiteur = request::input('visiteur');
        $visiteur = new Visiteur();
        $comptable = new Comptable();
        $idVisiteur = request::input('visiteur');
        $ligneVisiteur = $comptable->listeVisiteur();
        $listeMois = $visiteur->getMois($idVisiteur);
        $fiche = $visiteur->getFiche($date, $idVisiteur);
        $ligneForfait = $visiteur->getLigneForfait($date, $idVisiteur);
        $ligneHorsForfait = $visiteur->getLigneHorsForfait($date, $idVisiteur);
        $nbJustificatifs = $visiteur->getJustificatifs($idVisiteur, $date);
        $leVisiteur = $comptable->getLeVisiteur($idVisiteur);
        $dateLettre = $visiteur->dateLettre($date);
        return view('ValidationFiche', compact('ligneVisiteur', 'listeMois', 'ligneForfait', 'ligneHorsForfait', 'idVisiteur', 'leVisiteur', 'dateLettre', 'date', 'fiche','nbJustificatifs'));
    }

//    insertion des frais forfaité dans la base de donnéés
    public function actualiserForfait() {
        $etape = Request::input('etp');
        $kilo = Request::input('km');
        $nuit = Request::input('nui');
        $resto = Request::input('rep');
        $date = Request::input('mois');
        $idVisiteur = request::input('visiteur');
        $unComptable = new Comptable();

        $unComptable->updateFicheForfait($etape, $kilo, $nuit, $resto, $date, $idVisiteur);
        $view = $this->getFichesVisiteur();
        return $view;
    }

//    création de la session du comptable
    public function getCompte() {

        $comptable = new comptable();
        $id = Session::get('id');

        $compte = $comptable->getCompte($id);

        return view('CompteComptable', compact('compte'));
    }

//    validation de la fiche par le comptable
    public function validerFiche() {

        $idVisiteur = request::input('visiteur');
        $mois = Request::input('mois');
        $comptable = new comptable();
        $visiteur = new Visiteur();

        $ligneForfait = $visiteur->getLigneForfait($mois, $idVisiteur);
        $ligneHorsForfait = $visiteur->getLigneHorsForfait($mois, $idVisiteur);

        $err = $comptable->valFiche($idVisiteur, $mois, $ligneForfait, $ligneHorsForfait);
        $view = $this->getFichesVisiteur();
        return $view;
    }

//    affichage des fiches suivie par le comptable
    public function suivreFiche() {
        $visiteur = new Visiteur();
        $comptable = new Comptable();
        $listeFiche = $comptable->listerFiche();
        foreach ($listeFiche as $key => $value) {
            $mois = $visiteur->dateLettre($value->mois);
            $value->moislettre = $mois;
        }
        return view('SuivieFiche', compact('listeFiche'));
    }

//    changement de l'etat de la fiche a 'rembourser par le comptable
    public function setRembourser($mois, $id) {

        $comptable = new comptable();
        $comptable->RembourserFiche($mois, $id);

        $view = $this->suivreFiche();

        return $view;
    }

//    changement d'un frais hors forfait par le comptable'
    public function actualiserHorsForfait($id) {
        $date = Request::input('mois');
        $idVisiteur = Request::input('visiteur');

        $value = Request::input($id);

        $comptable = new Comptable();
        $comptable->actualiserHorsForfait($id, $value);

        $view = $this->getFichesVisiteur();
        return $view;
    }

//    suppression d'un frais hors forfait par le comptable'
    public function supprimerHorsForfait($id) {


        $comptable = new Comptable();
        $comptable->supprimerHorsForfait($id);

        $view = $this->getFichesVisiteur();
        return $view;
    }

//     report au mois suivant d'un frais hors forfait par le comptable'
    public function reporterHorsForfait($id) {

        $idVisiteur = request::input('visiteur');
        $mois = Request::input('mois');

        $visiteur = new Visiteur();
        $comptable = new Comptable();
        $comptable->reporterHorsForfait($id, $mois, $idVisiteur);
        $view = $this->getFichesVisiteur();
        return $view;
    }

}
