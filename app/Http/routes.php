<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    $erreur = "";
    return view('Accueil',compact('erreur'));
});
Route::post('/connexion','AccueilController@getConnexion');
Route::get('/deconnexion','VisiteurControlleur@getDeconnexion');
Route::get('/fichefrais','VisiteurControlleur@getFiche');
Route::get('/renseignerFiche','VisiteurControlleur@renseignerFiche');
Route::get('/accueilVisiteur','VisiteurControlleur@Accueil');
Route::post('/forfait','VisiteurControlleur@setForfait');
Route::post('/horsforfait','VisiteurControlleur@HorsForfait');
Route::post('/ficheMois','VisiteurControlleur@ficheMois');
Route::get('/consulterFiche','ComptableControlleur@consulterFiche');
Route::get('/accueilComptable','ComptableControlleur@Accueil');
Route::post('/rechercheFiche','ComptableControlleur@getMoisVisiteur');
Route::post('/afficheFiche','ComptableControlleur@getFichesVisiteur');
Route::post('/actualiserForfait','ComptableControlleur@actualiserForfait');
Route::post('/reinitialiserForfait','ComptableControlleur@getFichesVisiteur');
Route::get('//compteVisiteur','VisiteurControlleur@getCompte');
Route::get('/compteComptable','ComptableControlleur@getCompte');
Route::post('/ValiderFiche','ComptableControlleur@validerFiche');
Route::get('/suivreFiche','ComptableControlleur@suivreFiche');
Route::get('/rembourser/{mois}/{id}','ComptableControlleur@setRembourser');
Route::post('/actualiserHorsForfait/{id}','ComptableControlleur@actualiserHorsForfait');
Route::post('/supprimerHorsForfait/{id}','ComptableControlleur@supprimerHorsForfait');
Route::post('/reporterHorsForfait/{id}','ComptableControlleur@reporterHorsForfait');
Route::post('/justificatif','VisiteurControlleur@setJustificatif');
Route::get('/suppHorsForfait/{id}','VisiteurControlleur@suppHorsForfait');