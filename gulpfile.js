/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
var gulp = require('gulp');
var concat = require('gulp-concat');
var dir = './resources/assets';
var bower_dir = './bower_components';

gulp.task('css-dev', function() {
  return gulp.src([
        
        bower_dir + '/bootstrap/dist/css/bootstrap.css',
        bower_dir + '/bootstrap-material-design/dist/css/ripples.css',
        bower_dir + '/bootstrap-material-design/dist/css/bootstrap-material-design.css',
        dir + '/css/bootstrap-datetimepicker.css',
        dir + '/css/sticky-footer-navbar.css',
        dir + '/css/sidebar.css',
        dir + '/css/style.css'
    ])
    .pipe(concat('main.css'))
    .pipe(gulp.dest('./public/assets/css/'));
});

gulp.task('js-dev', function() {
  return gulp.src([
        bower_dir + '/jquery/dist/jquery.js',
        bower_dir + '/bootstrap/dist/js/bootstrap.min.js',
        bower_dir + '/bootstrap-material-design/dist/js/material.min.js',
        bower_dir + '/bootstrap-material-design/dist/js/ripples.min.js',
        dir + '/js/bootstrap-datetimepicker.js'
    ])
    .pipe(concat('main.js'))
    .pipe(gulp.dest('./public/assets/js/'));
});
