# Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, queueing, and caching.

Laravel is accessible, yet powerful, providing tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).

## Deploiment

### utilitaire nécessaire à l'installation de l'application 
Un serveur HTTP comme [Xampp](https://www.apachefriends.org/fr/index.html),<br>
Le SGBD [mysql](https://www.mysql.com/fr/downloads/), (inutile si Xampp est utilisé)<br>
Le Gestionnaire de paquet [Composer](https://getcomposer.org/),<br>
Un émulateur de console Comme [Commander](http://cmder.net/)

Apres avoir cloné l'application à la racine du serveur HTTP, ouvrez un fenêtre console dans le répertoire GSBFrais du serveur.<br>Pour installer les dépendances de Vendor taper la commande suivante  : ` $ composer install`<br>
Renomer dans le dossier GSBFrais le fichier *.env.example* en *.env*
De nouveau dans la console obtenez une nouvelle clé API en tapant `$ php artisan key:generate`
Utilisé le fichier DB.sql pour installé la base de donné dans votre SGBD<br>
Ouvrez le fichier *.env* avec un éditeur de texte Remplacer BD-CONNECTION par votre SGBD puis DB-DATABASE=gsb_frais enfin DB-USERNAME et DB-PASSWORD par votre identifiant et mot de passe pour avoir accès à la base de donnée.
Ouvrer votre navigateur internet et entrer l'URL suivante : *http://localhost/GSBFrais/public/index.php*


## Connexion
Pour les viviteurs et les Comptables
Pour vous connecter Login = cbedos et mot de passe = secret.