 <div id="wrapper">
        <div class="overlay"></div>
    
        <!-- Sidebar -->
        <nav class="navbar navbar-inverse" id="sidebar-wrapper" role="navigation">
            <ul class="nav sidebar-nav">
                <li class="sidebar-brand">
                   <h2>{{ Session::get('nom') }}<br>
                    {{ Session::get('prenom') }}
            
          </h2>
                </li>
                <li>
                   <a href="{{url('/accueilVisiteur')}}">Acceuil<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-home"></span></a>
                </li>                 
                <li>
                   <a href="{{url('/compteVisiteur')}}">Mon Compte<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-user"></span></a>
                </li>
                <li>
                   <a href="{{ url('/renseignerFiche')}}">Saisie fiche des frais<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-th-list"></span></a>
                </li>
                <li>
                    <a href="{{url('/fichefrais')}}">Mes fiches de frais<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-tags"></span></a>
                </li>
                <li>
                   <a href="{{url('/deconnexion')}}">Se déconnecter<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-off"></span></a>
                </li>
                
            </ul>
        </nav>
 </div>  
      
        <!-- /#sidebar-wrapper -->
        <script>
            $(document).ready(function () {
  var trigger = $('.hamburger'),
      overlay = $('.overlay'),
     isClosed = false;

    trigger.click(function () {
      hamburger_cross();      
    });

    function hamburger_cross() {

      if (isClosed == true) {          
        overlay.hide();
        trigger.removeClass('is-open');
        trigger.addClass('is-closed');
        isClosed = false;
      } else {   
        overlay.show();
        trigger.removeClass('is-closed');
        trigger.addClass('is-open');
        isClosed = true;
      }
  }
        
});
        </script>


