@extends('layouts.master')
@section('content')

<div class="row">
    <div class="col-md-2">
        @include('MenuVisiteur') 
    </div>

    <div class="row">   
        <div class="formatWell well bs-component  col-md-4 col-md-offset-2">  
            <div class="form-header form-header-size">
                <div class="alert alert-dismissible alert-info wellheader">
                    <h1><strong>Mes Fiches de Frais</strong></h1>
                </div>
            </div>
            {!! Form::open(['id' => 'moisForm', 'url' => 'ficheMois','id' => 'FicheForm', 'method' => 'POST']) !!} 
<!--            selection du mois pour la fiche de frais-->
            <label class="text-center control-label frmtxt">Mois à séléctionner :</label>
            <div class="form-group" id="selectmois">
                <div class="col-md-4 col-md-offset-3">
                    <select class="form-control" id='mois' name='mois'>
                        @foreach($lignes as $key => $ligne)
                        <option value="{{$key}}">{{ $ligne }}</option>
                        @endforeach
                    </select>
                </div>
                <input type="submit" class="btn btn-purple" id="ok" value="Valider"style="margin: 0px" />
            </div>
            <br>
<!--            affichage des frais forfaité-->
            @if($ligneForfait!=null)
            <div class="panel panel-info">
                <div class="panel-heading">
                    <label class="text-center control-label frmtxt">Fiche de Frais Forfait du mois de {{$dateLettre}}</label>
                </div>
                <div class="panel-body">
                    <div class="col-md-10 col-md-offset-1">
                        <br>
                        <table class="table table-striped table-hover">
                            <thead>
                                @foreach($ligneForfait as $key => $value)
                            <th class="text-center">{{$key}}</th>
                            @endforeach
                            </thead>
                            <tbody>
                                @foreach($ligneForfait as $key => $value)
                            <td class="text-center">{{$value}}</td>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @endif
<!--affichage des frais non forfaité-->
            @if($ligneHorsForfait!=null)
            <div class="panel panel-info">
                <div class="panel-heading">
                    <label class="text-center control-label frmtxt">Fiche de Frais Hors Forfait du mois de {{$dateLettre}}</label>
                </div>
                <div class="panel-body">
                    <div class="col-md-10 col-md-offset-1">
                        <br>
                        <table class="table table-striped table-hover">
                            <thead>
                            <th class="text-center">Libelle</th> 
                            <th class="text-center">Date</th> 
                            <th class="text-center">Montant</th> 
                            </thead>
                            <tbody>
                                @foreach($ligneHorsForfait as $key => $value)
                                <tr>
                                    <td class="text-center">{{$value->libelle}}</td>
                                    <td class="text-center">{{$value->date}}</td>
                                    <td class="text-center">{{$value->montant}}</td>
                                    <td><a onclick="supp()" href="{{ url ('suppHorsForfait') }}/{{$value->id}}" class="btn btn-raised btn-purple btn-xs"><span class="glyphicon glyphicon-remove">Supprimer</span></a> </td>
                                </tr>  
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>     
            @endif
<!--            affichage des justificatif-->
               @if($nbJustificatifs!=null)
            <div class="panel panel-info">
                <div class="panel-heading">
                    <label class="text-center control-label frmtxt">Nombre de justificatifs du mois de {{$dateLettre}}</label>
                </div>
                <div class="panel-body">
                    <label class="text-center control-label frmtxt">{{$nbJustificatifs[0]}}</label>
                </div>
            </div>
               @endif
            {!! Form::close() !!} 
        </div>
    </div>
</div>
@stop
