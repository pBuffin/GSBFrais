<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
       "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <title>Intranet du Laboratoire Galaxy-Swiss Bourdin</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    {!! Html::style('assets/css/main.css') !!}
        {!! Html::script('assets/js/main.js') !!}
  </head>
  <body>
<!--      header et logo GSB frais-->
      <div class="container-fluid" style="padding: 0px">
      <div class="row">
          <div class="well" style="margin: 0px">
              <h1 class="text-center">{{ Html::image('/../images/logo.jpg') }} 
                  Suivi du remboursement des frais</h1>
      </div>
      </div>
   </div> 
<!--fin header-->
      <div class="container-fluid">
            @yield('content')
       </div>
<!--      footer avec validation html-->
    <footer class="footer" style="border: solid 3px red">
        <div class="container center-block">
            <div class="">
                <img src="http://www.w3.org/Icons/valid-xhtml10" alt="Valid XHTML 1.0 Strict" />
                <img src="http://jigsaw.w3.org/css-validator/images/vcss" alt="CSS Valide !" />
                Cette page est conforme aux standards du Web
            </div>  
    </div>
    </footer>  
<!--fin footer-->
      <script>
//          Initalisation de material design pour le front
      $.material.init();     
      </script>
 </body>
</html>

