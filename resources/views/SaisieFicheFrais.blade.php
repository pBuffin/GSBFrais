@extends('layouts.master')
@section('content')
<div class="row">
    <div class="col-md-2">
        @include('MenuVisiteur') 
    </div>
    <div class="row">   
        <div class="formatWell well bs-component  col-md-4 col-md-offset-2">  
            <div class="form-header form-header-size">
                <div class="alert alert-dismissible alert-info wellheader">
                    <h1><strong>{{ $titre }}</strong></h1>
                </div>
            </div>
            {!! Form::open(['url' => 'forfait','id' => 'ForfaitForm']) !!}
            <div class="panel panel-info">
                <div class="panel-heading">
                    <label class="control-label text-center frmtxt">Nouvelle Eléments Forfaitisés</label>
                </div>
                <div class="panel-body">
                    <div class="form-group row" id='etapeDiv'>
                        <label for="Etape" class="col-md-4 control-label frmtxt" >Forfait Etape : </label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="etape" name='etape' title="Entrez le nombre d'etapes" placeholder="Nombre d'étape" data-toggle="tooltip" data-placement="right" />  
                        </div>
                    </div>
                    <div class="form-group row" id='kiloDiv' >
                        <label for="Kilometre" class="col-md-4 control-label frmtxt">Frais Kilometrique : </label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="kilo" name='kilo' title="Entrez le nombre de kilometre parcourus" placeholder="Nombre de kilometre parcouru" data-toggle="tooltip" data-placement="right"/>  
                        </div>
                    </div>

                    <div class="form-group row" id='nuitDiv' >
                        <label for="nuit" class="col-md-4 control-label  frmtxt">Nuitée Hôtel : </label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="nuit" name='nuit' title="Entrez le nombre de nuit d'hotel" placeholder="Nombre de nuit d'hotel" data-toggle="tooltip" data-placement="right"/>  
                        </div>
                    </div>
                    <div class="form-group row" id='restoDiv'>
                        <label for="resto" class="col-md-4 control-label frmtxt">Repas Restaurant : </label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="resto" name='resto' title="Entrez le nombre de repas" placeholder="Nombre de repas" data-toggle="tooltip" data-placement="right"/>  
                        </div>
                    </div>
                    <div class="row">
                        <div class="container col-md-offset-1">
                            <input type="submit" class="btn btn-purple" id="ok" value="Valider"style="margin-right: 10px" />
                            <input type="button" class="btn btn-purple" id="annuler" value="Effacer" />
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!} 
            {!! Form::open(['url' => 'horsforfait', 'id'=>'horsForfait']) !!}    
            <div class="panel panel-info">
                <div class="panel-heading">
                    <label class="control-label text-center frmtxt">Nouvelle élément hors forfait</label>
                </div>
                <div class="panel-body">
                    <div class="form-group row" id='DateDiv'>
                        <label for="Date" class="col-md-4 control-label frmtxt">Date : </label>
                        <div class="col-md-5">
                            <div class='input-group date' id='datetimepicker'>
                                <input type='text' class="form-control" name='date' id='date' title="Entrez la date" data-toggle="tooltip" data-placement="right"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <script type="text/javascript">
                        $(function () {
//                            initialisation du calendrier pour les frais hors forfait
                            $('#datetimepicker').datetimepicker({
                                format: 'dd/mm/yyyy',                           
                                minView: 2,
                                autoclose: true
                            });
                        });
                    </script>
                    <div class="form-group row" id='libelDiv'>
                        <label for="libelle" class="col-md-4 control-label frmtxt">Libellé : </label>
                        <div class="col-md-5">
                            <input type="text" class="form-control" id="libelle" name='libelle' title="Entrez le libellé de l'élémént" placeholder="libelle"/>  
                        </div>
                    </div>
                    <div class="form-group row" id='montDiv'>
                        <label for="montant" class="col-md-4 control-label frmtxt">Montant : </label>
                        <div class="col-md-5">
                            <input type="text" class="form-control" id="montant" name='montant' title="Entrez le montant" placeholder="montant" data-toggle="tooltip" data-placement="right"/>  
                        </div>
                    </div>
                   <div class="row">
                        <div class="container col-md-offset-1">
                            <input type="submit" class="btn btn-purple" id="ok" value="Ajouter"style="margin-right: 10px" />
                            <input type="button" class="btn btn-purple" id="annuler" value="Effacer" />
                        </div>
                    </div>
                </div>     
            </div>
            {!! Form::close() !!} 
             {!! Form::open(['url' => 'justificatif', 'id'=>'horsForfait']) !!}  
              <div class="panel panel-info">
                <div class="panel-heading">
                    <label class="control-label text-center frmtxt">Nombre de justificatifs</label>
                </div>
                  <div class="panel-body">
               <div class="form-group row" id='montDiv'>
                        <label for="justificatif" class="col-md-4 control-label frmtxt">Nombre de justificatif : </label>
                        <div class="col-md-5">
                            <input type="text" class="form-control" id="justificatif" name='justificatif'/>  
                        </div>
                    </div>
                    <div class="row">
                        <div class="container col-md-offset-1">
                            <input type="submit" class="btn btn-purple" id="ok" value="Ajouter"style="margin-right: 10px" />
                            <input type="button" class="btn btn-purple" id="annuler" value="Effacer" />
                        </div>
                    </div>
             </div>
             {!! Form::close() !!}
        </div>
    </div>

    <script type="text/javascript">

        $(document).ready(function () {
//            verificartion des champs avant validation
            $('#ForfaitForm').submit(function (e) {
                if (isNaN(($('#etape').val()))) {
                    $('#etape').tooltip('show');
                    e.preventDefault();
                } else {
                    $('#etape').tooltip('destroy');
                }
                if (isNaN(($('#kilo').val()))) {
                    $('#kilo').tooltip('show');
                    e.preventDefault();
                } else {
                    $('#kilo').tooltip('destroy');
                }
                if (isNaN(($('#nuit').val()))) {
                    $('#nuit').tooltip('show');
                    e.preventDefault();
                } else {
                    $('#nuit').tooltip('destroy');
                }
                if (isNaN(($('#resto').val()))) {
                    $('#resto').tooltip('show');
                    e.preventDefault();
                } else {
                    $('#resto').tooltip('destroy');
                }
                if ($('#resto').val() == "" && $('#nuit').val() == "" && $('#kilo').val() == "" && $('#etape').val() == "") {
                    e.preventDefault();
                }
            });

            $('#horsForfait').submit(function (e) {
                var vDate = verif();
                if (!vDate) {
                    $('#date').tooltip('show');
                    e.preventDefault();
                } else {
                    $('#date').tooltip('destroy');
                }
                if (isNaN(($('#montant').val())) || $('#montant').val() == "") {
                    $('#montant').tooltip('show');
                    e.preventDefault();
                } else {
                    $('#montant').tooltip('destroy');
                }
            });
        });

        function verif() {
//            verification qu'une date a bien été rentré dans le champs date des frais hors forfait
            var bool = false;
            var date = document.getElementById('date').value;
            var format = /(\d{1,2}\/){2}\d{4}$/;
            if (format.test(date)) {
                var date_temp = date.split('/');
                var ma_date = new Date();
            
                ma_date.setFullYear(date_temp[2]);
                ma_date.setMonth(date_temp[1]);
                ma_date.setDate(date_temp[0]);
                         
                if (ma_date.getFullYear() == date_temp[2] && ma_date.getMonth() == (parseInt(date_temp[1])) && ma_date.getDate() == (parseInt(date_temp[0]))) {
                    bool = true;
                }
                return bool;
            }
        }
    </script>
    @stop
