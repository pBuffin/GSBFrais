<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
@extends('layouts.master')
@section('content')
<div class="row">
    <div class="col-md-2">
        @include('MenuComptable') 
    </div>
    <div class="row">
        <div class="formatWell well bs-component  col-md-4 col-md-offset-2" >  
            <div class="form-header form-header-size">
                <div class="alert alert-dismissible alert-info wellheader">
                    <h1><strong>Bienvenue sur l'intranet GSB</strong></h1>
                </div>
            </div>
            {!! Form::open(['id' => 'visiteurForm', 'url' => 'rechercheFiche', 'class' => 'form-horizontal', 'method' => 'POST']) !!}  
            <div class="form-group">
                <label class="col-md-5 control-label text-center frmtxt ">Choisir le visiteur : </label> 
                <div class="col-md-5">
                    <select class="form-control" id='visiteur' name='visiteur'>
                        @foreach($ligneVisiteur as $ligne)
                        <option value="{{$ligne->id}}" <?php if ($ligne->id == $idVisiteur) echo "selected" ?>>{{ $ligne->prenom . " " . $ligne->nom}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            @if($listeMois !=null)         
            <div class="form-group">
                <label class="col-md-5 control-label text-center frmtxt" >Mois à séléctionner :</label>
                <div class="col-md-5">
                    <select class="form-control" id='mois' name='mois'>
                        <option>Mois</option>
                        @foreach($listeMois as $key => $ligne)
                        <option value="{{$key}}" <?php if ($date == $key) echo "selected" ?> >{{ $ligne }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            @endif
            @if($ligneForfait!=null) 
            <div class="panel panel-info">
                <div class="panel-heading">
                    <label class="control-label text-center frmtxt ">Fiche de Frais Forfait de {{$leVisiteur}} du mois de {{$dateLettre}}</label>
                </div>          
                <div class="panel-body">
                    <table class="table table-striped table-hover">
                        <thead>
                            @foreach($ligneForfait as $key => $value)
                        <th class="text-center">{{$key}}</th>
                        @endforeach
                        </thead>                          
                        <tbody> 
                            @if(isset($ligneForfait['Forfait Etape']))   
                        <td class="text-center">                                         
                            <div class="form-group" style="margin: 0px">                          
                                <input type="text" class="form-control text-center" id="etp" name="etp" value="{{$ligneForfait['Forfait Etape']}}" >                                                   
                            </div>                            
                        </td> 
                        @endif
                        @if(isset($ligneForfait['Frais Kilométrique']))
                        <td class="text-center">                                         
                            <div class="form-group" style="margin: 0px">                          
                                <input type="text" class="form-control text-center" id="km" name="km" value="{{$ligneForfait['Frais Kilométrique']}}" >                                                  
                            </div>                            
                        </td> 
                        @endif
                        @if(isset($ligneForfait['Nuitée Hôtel']))
                        <td class="text-center">                                         
                            <div class="form-group" style="margin: 0px">                          
                                <input type="text" class="form-control text-center" id="nui" name="nui" value="{{$ligneForfait['Nuitée Hôtel']}}" >                                                
                            </div>                            
                        </td>
                        @endif
                        @if(isset($ligneForfait['Repas Restaurant']))
                        <td class="text-center">                                         
                            <div class="form-group" style="margin: 0px">                          
                                <input type="text" class="form-control text-center" id="rep" name="rep" value="{{$ligneForfait['Repas Restaurant']}}" >                                                
                            </div>                            
                        </td>
                        @endif
                        @if($fiche->idEtat == 'CL')
                        <td class="col-md-5" style="padding: 0px">   
                            <div class="row">
                                <div class="container col-md-12">
                                    <a href="#" class="btn btn-raised btn-purple btn-xs" id="actuForfait"><span class="glyphicon glyphicon-refresh">Actualiser</span></a>                 
                                    <a href="#" class="btn btn-raised btn-purple btn-xs" id="reinitForfait"><span class="glyphicon glyphicon-repeat">Réinitialiser</span></a>                                    
                                </div>
                            </div>
                        </td> 
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
            @endif 
            @if($ligneHorsForfait!=null)
            <div class="panel panel-info">
                <div class="panel-heading">
                    <label class="control-label text-center frmtxt">Fiche de Frais Hors Forfait de {{$leVisiteur}} du mois de {{$dateLettre}}</label>
                </div>
                <div class="panel-body">  
                    <div>
                        <table class="table table-striped table-hover">
                            <thead>
                            <th class="text-center">Libelle</th> 
                            <th class="text-center">Date</th> 
                            <th class="text-center">Montant</th> 
                            </thead>
                            <tbody>
                                @foreach($ligneHorsForfait as $key => $value)
                                <tr>
                                    <td class="text-center">{{$value->libelle}}</td>
                                    <td class="text-center">{{$value->date}}</td>
                                    <td class="text-center"> 
                                        <div class="form-group" style="margin: 0px">  
                                            <input id="{{$value->id}}" name="{{$value->id}}" type="text" class="form-control text-center" value="{{$value->montant}}" >                                                
                                        </div> </td>
                                    @if($fiche->idEtat == 'CL')
                                    <td class="col-md-6" style="padding: 0px">   
                                        <div class="row">
                                            <div class="container col-md-12">
                                                <a onclick="actu({{$value->id}})" class="btn btn-raised btn-purple btn-xs"><span class="glyphicon glyphicon-refresh">Actualiser</span></a>                 
                                                <a href="#" id="reinitForfait" class="btn btn-raised btn-purple btn-xs"><span class="glyphicon glyphicon-repeat">Réinitialiser</span></a> 
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="container col-md-12">
                                                <a onclick="repo({{$value->id}})" class="btn btn-raised btn-purple btn-xs"><span class="glyphicon glyphicon-upload">Reporter</span></a>                 
                                                <a  onclick="supp({{$value->id}})" class="btn btn-raised btn-purple btn-xs"><span class="glyphicon glyphicon-remove">Supprimer</span></a> 
                                            </div>
                                        </div>
                                    </td>
                                    @endif
                                </tr>  
                                @endforeach
                            </tbody>
                        </table>
                    </div> 
                </div> 
            </div>
            @endif
            @if(($nbJustificatifs!=null))
            <div class="panel panel-info">
                <div class="panel-heading">
                    <label class="text-center control-label frmtxt">Nombre de justificatifs du mois de {{$dateLettre}} de {{$leVisiteur}}</label>
                </div>
                <div class="panel-body">
                    <label class="text-center control-label frmtxt">{{$nbJustificatifs[0]}}</label>
                </div>
            </div>
            @endif
<!--            on affiche l'état de la fiche en cours-->
            @if(($ligneHorsForfait!=null || $ligneForfait!=null))
            <?php
            switch ($fiche->idEtat) {
                case 'CR' :
                    ?>
                    <div>
                        <p>fiche en cours de saisie</p> 
                    </div>
                    <?php
                    break;
                case 'CL' :
                    ?>
                    <div>
                        <a href="#" id="validerFiche" class="btn btn-raised btn-purple">Valider cette fiche</a> 
                    </div>
                    <?php
                    break;
                case 'VA' :
                    ?>
                    <div>
                        <p>fiche déjà valider</p> 
                    </div>
                    <?php
                    break;
                case 'RB' :
                    ?>
                    <div>
                        <p>fiche rembourser</p> 
                    </div>
                    <?php
                    break;
            }
            ?>
            @endif
            {!! Form::close() !!} 
        </div>
    </div>
</div>

<script>
    $('#visiteur').change(function () {

    $('#visiteurForm').submit();
    });
    $('#mois').change(function () {
    document.getElementById("visiteurForm").action = '{{ url ('/afficheFiche') }}';
    $('#visiteurForm').submit();
    });
    $('#actuForfait').click(function(){
    document.getElementById("visiteurForm").action = '{{ url ('/actualiserForfait') }}';
    $('#visiteurForm').submit();
    });
    $('#reinitForfait').click(function(){
    document.getElementById("visiteurForm").action = '{{ url ('/reinitialiserForfait') }}';
    $('#visiteurForm').submit();
    });
    $('#validerFiche').click(function(){
    document.getElementById("visiteurForm").action = '{{ url ('/ValiderFiche') }}';
    $('#visiteurForm').submit();
    });
    function actu(id){

    var action = "{{ url ('/actualiserHorsForfait') }}/" + id;
    document.getElementById("visiteurForm").action = action
            $('#visiteurForm').submit();
    }
    function supp(id){

    var action = "{{ url ('supprimerHorsForfait') }}/" + id;
    document.getElementById("visiteurForm").action = action
            $('#visiteurForm').submit();
    }
    function repo(id){

    var action = "{{ url ('/reporterHorsForfait') }}/" + id;
    document.getElementById("visiteurForm").action = action
            $('#visiteurForm').submit();
    }

</script>
@stop