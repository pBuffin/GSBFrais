@extends('layouts.master')
@section('content')
<div class="row">
    <div class="col-md-2">
        @include('MenuComptable') 
    </div>
    <div class="row">
        <div class="formatWell well bs-component  col-md-5 col-md-offset-2" >  
            <div class="form-header form-header-size">
                <div class="alert alert-dismissible alert-info wellheader">
                    <h1><strong>Bienvenue sur l'intranet GSB</strong></h1>
                </div>
            </div>         
            <table class="table table-striped table-hover">
                <thead>                         
                <th class="text-center">Nom</th>
                <th class="text-center">Prénom</th>
                <th class="text-center">Mois</th>
                <th class="text-center">nbJustificatifs</th>
                <th class="text-center">montantValide</th>
                <th class="text-center">dateModif</th>
                <th class="text-center">Etat</th>
                </thead>
                <tbody>
                    @foreach($listeFiche as $key => $value)
                    <tr>
                        <td class="text-center">{{$value->nom}}</td>
                        <td class="text-center">{{$value->prenom}}</td>
                        <td class="text-center">{{$value->moislettre}}</td>
                        <td class="text-center">{{$value->nbJustificatifs}}</td>
                        <td class="text-center">{{$value->montantValide}}</td>
                        <td class="text-center">{{$value->dateModif}}</td>
                        <td class="text-center">{{$value->idEtat}}</td>
                        @if($value->idEtat == 'VA')
                        <td><a href="{{url('/rembourser')}}/{{ $value->mois }}/{{ $value->id}}" class="btn btn-raised btn-purple btn-xs" id="actuForfait"><span class="glyphicon glyphicon-euro">Rembourser</span></a></td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>         
        </div>
    </div>
</div>
@stop
