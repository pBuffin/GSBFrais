@extends('layouts.master')
@section('content')

<div class="formatWell well bs-component col-md-4 col-md-offset-4">
    <div class="form-header form-header-size">
        <div class="alert alert-dismissible alert-info wellheader">
            <h1><span class="glyphicon glyphicon-envelope"></span><strong> Connexion</strong></h1> 
        </div>
    </div>

    {!! Form::open(['id' => 'loginForm', 'class' => 'form-horizontal center-block', 'url' => '/connexion']) !!}
<!--    choix du visiteur ou du comptable-->
    <div class="form-group" id='choix'>
        <label for='role' class="col-md-4 control-label frmtxt">* Vous êtes :</label>
        <div class='col-md-5'>
            <select class="form-control" id='role' name='role'>
                <option id="vm" value='visiteur'>Visiteur médical</option>
                <option id="compt" value='comptable'>Comptable</option>
            </select>
        </div>
    </div>  
<!--champs pour l'identifiant-->
    <div class="form-group row" id='loginDiv'>
        <label for="login" class="col-md-4 control-label frmtxt" style="padding-top: 8px">* Login : </label>
        <div class="col-md-5">
            <input type="text" class="form-control" id="login" name='login' title="Entrez votre login" placeholder="login" data-toggle="tooltip" data-placement="right"/>  
        </div>
    </div>
<!--champs pour le mot de pass-->
    <div class="form-group row"id='passDiv'>
        <label for="password" class="col-md-4 control-label frmtxt" style="padding-top: 8px">* Mot de passe : </label>
        <div class="col-md-5">
            <input type="password" class="form-control has-error" id="password" name="password" title="Entrez votre mot de passe" placeholder="mot de passe" data-toggle="tooltip" data-placement="right"/>
        </div>
    </div> 
<!--bouton de validation -->
    <div class="row">
        <div class="container col-md-offset-3">
            <input type="submit" class="btn btn-purple" id="ok" value="Valider"style="margin-right: 10px" />
            <input type="button" class="btn btn-purple" id="annuler" value="Effacer" />
        </div>
    </div>
    @if($erreur != "")
    <div class="row">
        <div class="col-md-12">
            @include('Erreur') 
        </div>
    @endif
        {!! Form::close() !!} 
    </div>      
    <script>
        $.material.init();
//        Verification des diffrent champ, si invalide on affiche les tooltips
        document.getElementById("annuler").addEventListener("click", function(){
        $('#login').val("");
        $('#password').val("");
        });
        $(document).ready(function() {
        $('#loginForm').submit(function(e){
        if ($('#login').val() == ""){
        $('#loginDiv').addClass('has-error');
        $('#login').tooltip('show');
        e.preventDefault();
        } else{
        $('#loginDiv').removeClass('has-error');
        $('#login').tooltip('destroy');
        }
        if ($('#password').val() == ""){
        $('#passDiv').addClass('has-error');
        $('#password').tooltip('show');
        e.preventDefault();
        } else{
        $('#passDiv').removeClass('has-error');
        $('#password').tooltip('destroy');
        }
        });
        });
    </script>
    @stop
