@extends('layouts.master')
@section('content')

<div class="row">
    <div class="col-md-2">
        @include('MenuVisiteur') 
    </div>

    <div class="formatWell well bs-component  col-md-4 col-md-offset-2" style="margin-top: 100px">  
        <div class="form-header form-header-size">
            <div class="alert alert-dismissible alert-info wellheader">
                <h1><strong>Bienvenue sur l'intranet GSB</strong></h1>
            </div>
        </div>

        <div class="panel panel-info">
            <div class="panel-heading">
                <label class="control-label text-center frmtxt ">Compte de {{$compte->nom}} {{$compte->prenom}}</label>
            </div> 
            <div class="panel-body">
                <table class="table table-striped table-hover">
                    <tr>
                        <td>
                            Nom
                        </td>
                        <td>
                            {{$compte->nom}}
                        </td>
                    </tr>  
                    <tr>
                        <td>
                            Prénom
                        </td>
                        <td>
                            {{$compte->prenom}}
                        </td>
                    </tr>
                     <tr>
                        <td>
                           Login
                        </td>
                        <td>
                            {{$compte->login}}
                        </td>
                    </tr> 
                    <tr>
                        <td>
                            Adresse
                        </td>
                        <td>
                            {{$compte->adresse}}
                        </td>
                    </tr> 
                    <tr>
                        <td>
                            Code Postale
                        </td>
                        <td>
                            {{$compte->cp}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Ville
                        </td>
                        <td>
                            {{$compte->ville}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Date d'embauche
                        </td>
                        <td>
                            {{$compte->dateEmbauche}}
                        </td>
                    </tr>
                </table> 
            </div>
        </div> 
    </div>

    @stop

